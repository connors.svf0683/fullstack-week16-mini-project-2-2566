import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { createHead } from '@unhead/vue'


import App from './App.vue'
import router from './router'
//@boostrap
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.bundle.min.js'

//@boostrap Icon
import 'bootstrap-icons/font/bootstrap-icons.min.css'
const app = createApp(App)
const head = createHead();

app.use(head)
app.use(createPinia())
app.use(router)

app.mount('#app')
